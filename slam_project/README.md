# 'Sequential Adversarial Learning for Self-Supervised Deep Visual Odometry' Pytorch version
This codebase implements the system described in the paper:

Sequential Adversarial Learning for Self-Supervised Deep Visual Odometry

Original Author : Shunkai Li 

Pytorch implementation by : Akankshya Kar, Sajal Maheswari, Shamit Lal, Vinay Sameer Raja Kadi

It is built on Pytorch implementation of [SFMlearner](https://github.com/ClementPinard/SfmLearner-Pytorch)

![sample_results](misc/all_nl.png)

## Preamble
This codebase was developed and tested with Pytorch 1.0.1, CUDA 10 and Ubuntu 16.04.

## Prerequisite

```bash
pip3 install -r requirements.txt
```

or install manually the following packages :

```
pytorch >= 1.0.1
pebble
matplotlib
imageio
scipy
argparse
tensorboardX
blessings
progressbar2
path.py
```


## Preparing training data
Preparation is roughly the same command as in the original code.

For [KITTI](http://www.cvlibs.net/datasets/kitti/raw_data.php), first download the dataset using this [script](http://www.cvlibs.net/download.php?file=raw_data_downloader.zip) provided on the official website, and then run the following command. The `--with-depth` option will save resized copies of groundtruth to help you setting hyper parameters. The `--with-pose` will dump the sequence pose in the same format as Odometry dataset (see pose evaluation)
```bash
python3 data/prepare_train_data.py /path/to/raw/kitti/dataset/ --dataset-format 'kitti' --dump-root /path/to/resulting/formatted/data/ --width 416 --height 128 --num-threads 4 [--static-frames /path/to/static_frames.txt] [--with-depth] [--with-pose]
```


## Training
Once the data are formatted following the above instructions, you should be able to train the model by running the following commands, based on which ablation experiment you want to run:
Note: Here, the ablation symbols mean the following:

B -> Using the components in the baseline code of SFMLearner

C -> Using optical flow codes

L -> Using LSTM for sequential processing

TC -> Using trajectory loss

G -> Using GAN (can be either WGAN or PatchGAN based on argument passed. Default is WGAN)



B+C+L:
```bash
python train.py <path_to_kitti_processed>  -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0 -t=0 -f=1000 --use_lstm --use_batchnorm
```

B+C+G WGAN:
```bash
python3 train.py <path_to_kitti_processed> -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -f=1000 --use_batchnorm
```

B+C+G PatchGAN:
```bash
python3 train.py <path_to_kitti_processed> -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -f=1000 --discriminator_type PatchGAN --use_batchnorm
```

B+C+G+L PatchGAN:
```bash
python3 train.py <path_to_kitti_processed>  -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0 -f=1000 --use_lstm --discriminator_type PatchGAN --use_batchnorm
```

B+C+G+L WGAN
```bash
python3 train.py  <path_to_kitti_processed>   -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0 -f=1000 --use_lstm --use_batchnorm
```

B+C+G+L+TC WGAN:
```bash
python3 train.py <path_to_kitti_processed> -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0.003 -f=1000 --use_lstm --use_batchnorm
```

B+C+G+L+TC PatchGAN:
```bash
python3 train.py <path_to_kitti_processed> -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0.003 -f=1000 --use_lstm --discriminator_type PatchGAN --use_batchnorm
```

You can then start a `tensorboard` session in this folder by
```bash
tensorboard --logdir=checkpoints/
```
and visualize the training progress by opening [https://localhost:6006](https://localhost:6006) on your browser. If everything is set up properly, you should start seeing reasonable depth prediction after ~30K iterations when training on KITTI.


## Evaluation

For evaluation, use the same commands as given in training section, but add an additional '--pretrained-net' argument with the path to the trained model.
Use the flag '--eval-poses' for pose evaluation.


### Results 
Results can be found in our submitted [report](https://drive.google.com/file/d/1-k8QGhRMdAmorwv11vOjdZKm41L408DG/view?usp=sharing) and [presentation](https://docs.google.com/presentation/d/1v-CvuykH5lAnMHvShE6LW1GMgkTFEg6Kf81ED2x4WDc/edit?usp=sharing) 