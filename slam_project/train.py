import argparse
import time
import csv
import os
import torch
from torch.autograd import Variable
import numpy as np
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import custom_transforms
import models
import utils
from utils import tensor2array, save_checkpoint, save_path_formatter, log_output_tensorboard
import pdb
from loss_functions import photometric_reconstruction_loss, explainability_loss, smooth_loss, compute_errors
from logger import TermLogger, AverageMeter
from tensorboardX import SummaryWriter
import torchvision
import ipdb 
st=  ipdb.set_trace
torch.manual_seed(125)
torch.cuda.manual_seed_all(125) 
torch.backends.cudnn.deterministic = True
'''
Kar (B+C+G+L) : CUDA_VISIBLE_DEVICES=1 python3 train.py /media/mscv/HDD/SLAM_datset/formatted-kitti -b4 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -s0.001 -g=0.01 -t=0 -f=1000 --use_lstm --use_batchnorm
Kar (B+C+G+L) PatchGAN : CUDA_VISIBLE_DEVICES=0 python3 train.py /media/mscv/HDD/SLAM_datset/formatted-kitti -b4 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -s0.001 -g=0.01 -t=0 -f=1000 --use_lstm --discriminator_type PatchGAN --use_batchnorm

=======
Sameer (B+C+L): CUDA_VISIBLE_DEVICES=3 python train.py /home/scratch/vkadi/kitti_data_processed/  -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0 -t=0 -f=1000 --use_lstm
Shamit (B+C+G+L+TC): python3 train.py path -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0.14 -f=1000 --use_lstm
Sajal: (B+C+G) python3 train.py path -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0.14 -f=1000
Shamit (B+C+G+L+TC) PatchGAN: python3 train.py path -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0.14 -f=1000 --use_lstm --discriminator_type PatchGAN
Sajal: (B+C+G) PatchGAN: python3 train.py path -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0.14 -f=1000 --discriminator_type PatchGAN
Kar (B+C+G+L) PatchGAN: CUDA_VISIBLE_DEVICES=2 python3 train.py /media/mscv/HDD/dataset/akankshya_dumps/slam_datset/formatted-kitti  -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0 -f=1000 --use_lstm --discriminator_type PatchGAN
Kar (B+C+G+L) : CUDA_VISIBLE_DEVICES= python3 train.py /media/mscv/HDD/dataset/akankshya_dumps/slam_datset/formatted-kitti  -b4 -s0.1 --epoch-size=3000 --sequence-length=15 --log-output --with-gt -g=0.01 -t=0 -f=1000 --use_lstm  --log-dir /media/mscv/HDD/dataset/akankshya_dumps/slam_datset/checkpoints

'''

'''
CUDA_VISIBLE_DEVICES=1 python train.py /home/scratch/vkadi/kitti_data_processed/ -b4 -m0.2 -s0.1 --epoch-size 100 -g=0.001 --log-dir /home/scratch/vkadi/slam_project_logs/
'''
parser = argparse.ArgumentParser(description='Structure from Motion Learner training on KITTI',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('data', metavar='DIR',
                    help='path to dataset')
# parser.add_argument('--training-output-freq', type=int, metavar='N', help='output freq for training', default=4)
# parser.add_argument('--print-freq', type=int, metavar='N', help='Print freq for training', default=2)
parser.add_argument('--dataset-format', default='sequential', metavar='STR',
                    help='dataset format, stacked: stacked frames (from original TensorFlow code) \
                    sequential: sequential folders (easier to convert to with a non KITTI/Cityscape dataset')
parser.add_argument('--sequence-length', type=int, metavar='N', help='sequence length for training', default=15)
parser.add_argument('--rotation-mode', type=str, choices=['euler', 'quat'], default='euler',
                    help='rotation mode for PoseExpnet : euler (yaw,pitch,roll) or quaternion (last 3 coefficients)')

parser.add_argument('--expname', type=str, default='exp1', help='experiment name')
parser.add_argument('--padding-mode', type=str, choices=['zeros', 'border'], default='zeros',
                    help='padding mode for image warping : this is important for photometric differenciation when going outside target image.'
                         ' zeros will null gradients outside target image.'
                         ' border will only null gradients of the coordinate outside (x or y)')
parser.add_argument('--with-gt', action='store_true', help='use ground truth for validation. \
                    You need to store it in npy 2D arrays see data/kitti_raw_loader.py for an example')
parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers')
parser.add_argument('--epochs', default=200, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--epoch-size', default=500, type=int, metavar='N',
                    help='manual epoch size (will match dataset size if not set)')
parser.add_argument('-b', '--batch-size', default=1, type=int,
                    metavar='N', help='mini-batch size')
parser.add_argument('--lr', '--learning-rate', default=1e-4, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum for sgd, alpha parameter for adam')
parser.add_argument('--beta', default=0.99, type=float, metavar='M',
                    help='beta parameters for adam')
parser.add_argument('--weight-decay', '--wd', default=3e-4, type=float,
                    metavar='W', help='weight decay')
parser.add_argument('--print-freq', default=10, type=int,
                    metavar='N', help='print frequency')
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained-net', dest='pretrained_net', default=None, metavar='PATH',
                    help='path to pre-trained model')
parser.add_argument('--eval-train', dest='eval_train', action='store_true',
                    help='evaluation on train dataset')
parser.add_argument('--pretrained-disp', dest='pretrained_disp', default=None, metavar='PATH',
                    help='path to pre-trained dispnet model')
parser.add_argument('--pretrained-exppose', dest='pretrained_exp_pose', default=None, metavar='PATH',
                    help='path to pre-trained Exp Pose net model')
parser.add_argument('--seed', default=0, type=int, help='seed for random functions, and network initialization')
parser.add_argument('--log-dir', default='./checkpoints', metavar='PATH',
                    help='where to save logs')
parser.add_argument('--log-summary', default='progress_log_summary.csv', metavar='PATH',
                    help='csv where to save per-epoch train and valid stats')
parser.add_argument('--log-full', default='progress_log_full.csv', metavar='PATH',
                    help='csv where to save per-gradient descent train stats')
# args.gan_loss_weight
parser.add_argument('-a', '--appearance-loss-weight', type=float, help='weight for appearance loss', metavar='W', default=0.75)
parser.add_argument('-s', '--smooth-loss-weight', type=float, help='weight for disparity smoothness loss', metavar='W', default=0.1)
parser.add_argument('-t', '--trajectory-loss-weight', type=float, help='weight for trajectory loss', metavar='W', default=0.14)
parser.add_argument('-g', '--gan-loss-weight', type=float, help='weight for GAN loss', metavar='W', default=0.01)
parser.add_argument('--log-output', action='store_true', help='will log dispnet outputs and warped imgs at validation step')
parser.add_argument('-f', '--training-output-freq', type=int, help='frequence for outputting dispnet outputs and warped imgs at training for all scales if 0 will not output',
                    metavar='N', default=0)
parser.add_argument('--discriminator_type', type = str, help = 'Select discriminator to use', choices = ['PatchGAN', 'Encoder'], default = 'Encoder')
parser.add_argument('--use_lstm', action='store_true', help='will use LSTM network for codes')

parser.add_argument('--use_smoothlossSFM', action='store_true', help = 'Select SFMlearner smotth function')
parser.add_argument('--use_batchnorm', action='store_true', help='Use batchnorm in encoder code')

best_error = -1
n_iter = 0
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
adversarial_loss = torch.nn.BCELoss()
img_shape = (3, 128, 416)
debug = False

def main():
    global best_error, n_iter, device, scheduler
    args = parser.parse_args()
    if args.use_batchnorm:
        print("Will use batchnorm")

    if args.dataset_format == 'stacked':
        from datasets.stacked_sequence_folders import SequenceFolder
    elif args.dataset_format == 'sequential':
        from datasets.sequence_folders import SequenceFolder
    save_path = save_path_formatter(args, parser)
    args.save_path = os.path.join(args.log_dir,save_path)
    print('=> will save everything to {}'.format(args.save_path))
    if debug:
        st()
    args.save_path.makedirs_p()
    torch.manual_seed(args.seed)
    if args.evaluate:
        args.epochs = 0

    tb_writer = SummaryWriter(args.save_path)
    # Data loading code
    normalize = custom_transforms.Normalize(mean=[0.5, 0.5, 0.5],
                                            std=[0.5, 0.5, 0.5])
    train_transform = custom_transforms.Compose([
        # torchvision.transforms.Resize((128,416)),
        custom_transforms.RandomHorizontalFlip(),
        # custom_transforms.RandomScaleCrop(),
        custom_transforms.ArrayToTensor(),
        normalize
    ])

    valid_transform = custom_transforms.Compose([custom_transforms.ArrayToTensor(), normalize])

    print("=> fetching scenes in '{}'".format(args.data))
    if debug:
        st()
    train_set = SequenceFolder(
        args.data,
        transform=train_transform,
        seed=args.seed,
        train=True,
        sequence_length=args.sequence_length
    )

    # if no Groundtruth is avalaible, Validation set is the same type as training set to measure photometric loss from warping
    #if args.with_gt:
    from datasets.validation_seq_folders import ValidationSet
    from datasets.train_seq_folders import TrainValidationSet
    full_seq = False
    if args.pretrained_net:
        full_seq = True

    val_set_gt = ValidationSet(
        args.data,
        transform=valid_transform,
        full_seq = full_seq
    )
    if args.eval_train:
        print("Evaluating on training dataset")
        val_set_gt = TrainValidationSet(
            args.data,
            transform=valid_transform,
            full_seq = full_seq
        )
    #else:
    val_set = SequenceFolder(
        args.data,
        transform=valid_transform,
        seed=args.seed,
        train=False,
        sequence_length=args.sequence_length,
    )
    print('{} samples found in {} train scenes'.format(len(train_set), len(train_set.scenes)))
    print('{} samples found in {} valid scenes'.format(len(val_set), len(val_set.scenes)))
    if debug:
        st()
    train_loader = torch.utils.data.DataLoader(
        train_set, batch_size=args.batch_size, shuffle=True,
        num_workers=args.workers, pin_memory=True)
    val_loader = torch.utils.data.DataLoader(
        val_set, batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)
    val_loader_gt = torch.utils.data.DataLoader(
        val_set_gt, batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)
    if args.epoch_size == 0:
        args.epoch_size = len(train_loader)

    # create model
    print("=> creating models")
    if debug:
        st()
    lstmNet = models.LSTMRunner(args)
    output_exp = args.appearance_loss_weight > 0
    if not output_exp:
        print("=> no mask loss, PoseExpnet will only output pose")
    cudnn.benchmark = True
    print('=> setting solvers')
    if debug:
        st()

    if args.discriminator_type == "Encoder": #SGD for encoder discriminator
        optim_params_disc = [{'params': lstmNet.discriminator.parameters(), 'lr':args.lr}]
        optimizer_disc = torch.optim.SGD(optim_params_disc,weight_decay=args.weight_decay)
        scheduler_disc = torch.optim.lr_scheduler.StepLR(optimizer_disc, 15000, gamma=0.5)
    else:
        optim_params_disc = [{'params': lstmNet.discriminator.parameters(), 'lr': args.lr}]
        optimizer_disc = torch.optim.Adam(optim_params_disc,betas=(args.momentum, args.beta),weight_decay=args.weight_decay)
        scheduler_disc = torch.optim.lr_scheduler.StepLR(optimizer_disc, 15000, gamma=0.5)
    #adam for pose and disp and code
    optim_params_others = [{'params': list(lstmNet.lstm.parameters()) + list(lstmNet.disp_net.parameters()) + list(lstmNet.pose_exp_net.parameters()) + list(lstmNet.encoder.parameters()), 'lr':args.lr}]
    optimizer_others = torch.optim.Adam(optim_params_others,betas=(args.momentum, args.beta),weight_decay=args.weight_decay)
    scheduler_others = torch.optim.lr_scheduler.StepLR(optimizer_others, 15000, gamma=0.5)

    logger = TermLogger(n_epochs=args.epochs, train_size=min(len(train_loader), args.epoch_size), valid_size=len(val_loader))
    logger.epoch_bar.start()

    if args.pretrained_net or args.evaluate:
        logger.reset_valid_bar()
        checkpoint = torch.load(args.pretrained_net)
        lstmNet.load_state_dict(checkpoint['model_state_dict'])
        errors, error_names = validate_with_gt(args, val_loader_gt, lstmNet, 0, logger, tb_writer)
        '''for error, name in zip(errors, error_names):
            tb_writer.add_scalar(name, error, 0)
        error_string = ', '.join('{} : {:.3f}'.format(name, error) for name, error in zip(error_names[2:9], errors[2:9]))
        logger.valid_writer.write(' * Avg {}'.format(error_string))'''
        for val, name in zip(errors, error_names):
            print(name, ":", val)
        return

    print("Training...")
    if debug:
        st()
    for epoch in range(args.epochs):
        logger.epoch_bar.update(epoch)

        # train for one epoch
        logger.reset_train_bar()
        train_loss = train(args, train_loader, lstmNet, optimizer_disc, optimizer_others, args.epoch_size, logger, tb_writer, scheduler_disc, scheduler_others)
        logger.train_writer.write(' * Avg Loss : {:.3f}'.format(train_loss))

        # evaluate on validation set
        logger.reset_valid_bar()
        if epoch%1==0: # TODO: fix this for lstm code
            print("Starting Validation")
            if debug:
                st()
            # if (epoch+2)%4==0:
            errors, error_names = validate_with_gt(args, val_loader_gt, lstmNet, epoch, logger, tb_writer)
            # else:
            #     errors, error_names = validate_without_gt(args, val_loader, lstmNet, epoch, logger, tb_writer)
        error_string = ', '.join('{} : {:.3f}'.format(name, error) for name, error in zip(error_names, errors))
        logger.valid_writer.write(' * Avg {}'.format(error_string))

        for error, name in zip(errors, error_names):
            tb_writer.add_scalar(name, error, epoch)

        # Up to you to chose the most relevant error to measure your model's performance, careful some measures are to maximize (such as a1,a2,a3)
        decisive_error = errors[1]
        if best_error < 0:
            best_error = decisive_error

        # remember lowest error and save checkpoint
        is_best = decisive_error < best_error
        best_error = min(best_error, decisive_error)
        save_checkpoint(args.save_path, lstmNet.state_dict(),is_best, epoch, args.expname + '_disc', optimizer_disc)
        save_checkpoint(args.save_path, lstmNet.state_dict(),is_best, epoch, args.expname + '_others', optimizer_others)

    logger.epoch_bar.finish()


def train(args, train_loader, lstmNet, optimizer_disc, optimizer_others, epoch_size, logger, tb_writer, scheduler_disc, scheduler_others):
    global n_iter, device
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter(precision=4)
    w1, w2, w3 ,w4 = args.appearance_loss_weight, args.smooth_loss_weight, args.trajectory_loss_weight, args.gan_loss_weight

    lstmNet.train()

    end = time.time()
    logger.train_bar.update(0)
  
    for i, (tgt_img, ref_imgs, intrinsics, intrinsics_inv) in enumerate(train_loader):
        # st()
        log_losses = i > 0 and n_iter % args.print_freq == 0
        log_output = args.training_output_freq > 0 and n_iter % args.training_output_freq == 0

        # measure data loading time
        data_time.update(time.time() - end)
        tgt_img = tgt_img.to(device)
        ref_imgs = [img.to(device) for img in ref_imgs]
        # pdb.set_trace()
        intrinsics = intrinsics.to(device)

        loss, loss_1, loss_2, loss_3, gan_loss = lstmNet(tgt_img, ref_imgs, intrinsics, log_losses, log_output, tb_writer, n_iter, False,\
                                                          mode ='train')
        # pdb.set_trace()
        # record loss and EPE
        losses.update(loss.item(), args.batch_size)

        # compute gradient and do step

        optimizer_others.zero_grad()
        optimizer_disc.zero_grad()
        loss.backward()
        optimizer_others.step()
        scheduler_others.step()
        optimizer_disc.step()
        scheduler_disc.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        logger.train_bar.update(i+1)
        if i % args.print_freq == 0:
            logger.train_writer.write('Train: Time {} Data {} Loss {}'.format(batch_time, data_time, losses))
        if i >= epoch_size - 1:
            break

        n_iter += 1

    return losses.avg[0]



@torch.no_grad()
def validate_without_gt(args, val_loader, lstmNet, epoch, logger, tb_writer, sample_nb_to_log=3):
    global device
    batch_time = AverageMeter()
    losses = AverageMeter(i=5, precision=4)
    log_outputs = sample_nb_to_log > 0
    w1, w2, w3 ,w4 = args.appearance_loss_weight, args.smooth_loss_weight, args.trajectory_loss_weight, args.gan_loss_weight
    poses = np.zeros(((len(val_loader)-1) * args.batch_size * (args.sequence_length-1),6))
    disp_values = np.zeros(((len(val_loader)-1) * args.batch_size * 3))

    # switch to evaluate mode
    lstmNet.eval()
    disp_net = lstmNet.disp_net
    pose_exp_net = lstmNet.pose_exp_net

    end = time.time()
    logger.valid_bar.update(0)
    for i, (tgt_img, ref_imgs, intrinsics, intrinsics_inv) in enumerate(val_loader):
        tgt_img = tgt_img.to(device)
        ref_imgs = [img.to(device) for img in ref_imgs]
        intrinsics = intrinsics.to(device)
        intrinsics_inv = intrinsics_inv.to(device)

        loss, loss_1, loss_2, loss_3, gan_loss = lstmNet(tgt_img, ref_imgs, intrinsics, False, False, tb_writer, n_iter, False, mode ='val')

        loss = w1*loss_1 + w2*loss_2 + w3*loss_3 + w4*gan_loss
        losses.update([loss, loss_1, loss_2, loss_3, gan_loss])

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()
        logger.valid_bar.update(i+1)
        if i % args.print_freq == 0:
            logger.valid_writer.write('valid: Time {} Loss {}'.format(batch_time, losses))

    logger.valid_bar.update(len(val_loader))
    return losses.avg, ['Total loss', 'Appearance loss', 'Depth smoothness loss', 'Trajectory loss', 'Gan loss']


@torch.no_grad()
def validate_with_gt(args, val_loader, lstmNet, epoch, logger, tb_writer, sample_nb_to_log=3):
    global device
    batch_time = AverageMeter()
    error_names = ['abs_diff', 'abs_rel', 'sq_rel', 'a1', 'a2', 'a3']
    errors = AverageMeter(i=len(error_names))
    log_outputs = sample_nb_to_log > 0

    # switch to evaluate mode
    lstmNet.eval()
    # st()
    end = time.time()
    logger.valid_bar.update(0)
    count=0
    for i, (ref_imgs, depth, intrinsics) in enumerate(val_loader):
        # count+=1
        # if count>10:
        #     break
        ref_imgs = [img.to(device) for img in ref_imgs]
        if args.pretrained_net:
            depth = [curr_depth.to(device) for curr_depth in depth]
        else:
            depth = depth.to(device)

        # compute output
        output_depth = lstmNet(depth, ref_imgs, intrinsics, False, False, tb_writer, n_iter, True, mode ='val') #[14, 4, 1, 128, 416]
        output_depth = output_depth[0] #[1, 128, 416]
        output_disp = 1.0/output_depth

        #output_depth = 1/output_disp[:,0]

        if log_outputs and i < sample_nb_to_log:
            if epoch == 0:
                tb_writer.add_image('val Input/{}'.format(i), tensor2array(ref_imgs[-1][0,:,:,:]), 0)
                if(args.pretrained_net):
                    depth_to_show = depth[-1][0]
                else:
                    depth_to_show = depth[0]
                tb_writer.add_image('val target Depth Normalized/{}'.format(i),
                                    tensor2array(depth_to_show, max_value=None),
                                    epoch)
                depth_to_show[depth_to_show == 0] = 1000
                disp_to_show = (1/depth_to_show).clamp(0,10)
                tb_writer.add_image('val target Disparity Normalized/{}'.format(i),
                                    tensor2array(disp_to_show, max_value=None, colormap='magma'),
                                    epoch)
                # st()
            pred_depth = output_depth[-1,:,:,:,:][0]
            pred_disp = 1/pred_depth
            tb_writer.add_image('val Dispnet Output Normalized/{}'.format(i),
                                tensor2array(pred_disp[0], max_value=None, colormap='magma'),
                                epoch)
            tb_writer.add_image('val Depth Output Normalized/{}'.format(i),
                                tensor2array(pred_depth[0], max_value=None),
                                epoch)

        if args.pretrained_net:
            depth = [elt.unsqueeze(0) for elt in depth]
            depth = torch.cat(depth, dim=0)
            depth = utils.pack_seqdim(depth, args.batch_size)
        output_depth = utils.pack_seqdim(output_depth, args.batch_size)

        errors.update(compute_errors(depth, output_depth[:,0,:,:]))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()
        logger.valid_bar.update(i+1)
        if i % args.print_freq == 0:
            logger.valid_writer.write('valid: Time {} Abs Error {:.4f} ({:.4f})'.format(batch_time, errors.val[0], errors.avg[0]))
    logger.valid_bar.update(len(val_loader))

    return errors.avg, error_names


if __name__ == '__main__':
    main()
