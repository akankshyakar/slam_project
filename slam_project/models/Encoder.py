import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.init import xavier_uniform_, zeros_
import ipdb 
st = ipdb.set_trace

def downsample_conv(in_planes, out_planes, args, kernel_size=3, lastlayer=False):
    if not args.use_batchnorm:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=2, padding=(kernel_size-1)//2),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_planes, out_planes, kernel_size=kernel_size, padding=(kernel_size-1)//2),
            nn.ReLU(inplace=True)
        )
    else:
        if not lastlayer:
            return nn.Sequential(
                nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=2, padding=(kernel_size-1)//2),
                nn.ReLU(inplace=True),
                nn.BatchNorm2d(out_planes),
                nn.Conv2d(out_planes, out_planes, kernel_size=kernel_size, padding=(kernel_size-1)//2),
                nn.ReLU(inplace=True),
                nn.BatchNorm2d(out_planes)
            )
        else:
            return nn.Sequential(
                nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=2, padding=(kernel_size-1)//2),
                nn.ReLU(inplace=True),
                nn.BatchNorm2d(out_planes),
                nn.Conv2d(out_planes, out_planes, kernel_size=kernel_size, padding=(kernel_size-1)//2),
                nn.ReLU(inplace=True)
            )



class Encoder(nn.Module):
    def __init__(self, args):
        super(Encoder, self).__init__()

        conv_planes = [16, 32, 64, 128, 128, 128, 128]
        self.conv1 = downsample_conv(2,              conv_planes[0], args, kernel_size=7)
        self.conv2 = downsample_conv(conv_planes[0], conv_planes[1], args, kernel_size=5)
        self.conv3 = downsample_conv(conv_planes[1], conv_planes[2], args)
        self.conv4 = downsample_conv(conv_planes[2], conv_planes[3], args)
        self.conv5 = downsample_conv(conv_planes[3], conv_planes[4], args)
        self.conv6 = downsample_conv(conv_planes[4], conv_planes[5], args, lastlayer=True)
        self.pool = nn.AdaptiveAvgPool2d(1)
        # self.conv7 = downsample_conv(conv_planes[5], conv_planes[6])

    def init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
                xavier_uniform_(m.weight)
                if m.bias is not None:
                    zeros_(m.bias)

    def forward(self, x):
        out_conv1 = self.conv1(x)
        out_conv2 = self.conv2(out_conv1)
        out_conv3 = self.conv3(out_conv2)
        out_conv4 = self.conv4(out_conv3)
        out_conv5 = self.conv5(out_conv4)
        out_conv6 = self.conv6(out_conv5)
        out = self.pool(out_conv6)
        return out.reshape(out.shape[0], out.shape[1])
