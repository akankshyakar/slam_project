import numpy as np
import cv2
import time
import os
import torch


def extract_OptFlow_batch(images_t, images_tp1):
    flow_batch = []
    for samp_idx in range(len(images_t)):
        img_t = cv2.cvtColor(images_t[samp_idx,:,:,:], cv2.COLOR_BGR2GRAY)
        img_tp1 = cv2.cvtColor(images_tp1[samp_idx,:,:,:], cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(img_t, img_tp1, None, 0.5, 3, 15, 3, 5, 1.2, 0)
        flow_batch.append(flow)
    flow_batch = np.array(flow_batch)
    flow_batch = np.transpose(flow_batch, (0,3,1,2))
    return flow_batch

def extract_OptFlow(image_seq):
    image_seq_np = image_seq.numpy()
    image_seq_np = np.transpose(image_seq_np, (0,1,3,4,2))
    image_seq_np = image_seq_np.astype(np.float32)
    opt_flow_seq = []
    for i in range(len(image_seq_np)-1):
        images_t = image_seq_np[i,:,:,:,:]
        images_tp1 = image_seq_np[i+1,:,:,:,:]
        opt_flow_t = extract_OptFlow_batch(images_t, images_tp1)
        opt_flow_seq.append(opt_flow_t)
    opt_flow_seq = torch.Tensor(np.asarray(opt_flow_seq))
    return opt_flow_seq

if __name__ == '__main__':
    # Testing code for optical flow extractor
    datapath = '/zfsauton2/home/vkadi/Downloads'
    img1 = cv2.imread(os.path.join(datapath, 'car1.jpg'))
    img2 = cv2.imread(os.path.join(datapath, 'car2.jpg'))

    img1 = np.transpose(img1,(2,0,1))
    img2 = np.transpose(img2,(2,0,1))

    img1 = np.array([img1, img1, img1, img1])
    img2 = np.array([img2, img2, img2, img2])

    img_seq = np.array([img1,img2, img1])
    
    print(img_seq.shape)
    opt_flow_seq = extract_OptFlow(torch.Tensor(img_seq))
    print(opt_flow_seq.shape)
