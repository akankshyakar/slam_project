import torch.utils.data as data
import numpy as np
from imageio import imread
from path import Path

def load_as_float(path):
    return imread(path).astype(np.float32)


class ValidationSet(data.Dataset):
    """A sequence data loader where the files are arranged in this way:
        root/scene_1/0000000.jpg
        root/scene_1/0000000.npy
        root/scene_1/0000001.jpg
        root/scene_1/0000001.npy
        ..
        root/scene_2/0000000.jpg
        root/scene_2/0000000.npy
        .

        transform functions must take in a list a images and a numpy array which can be None
    """

    def __init__(self, root, sequence_length=15, transform=None, full_seq = False):
        self.root = Path(root)
        scene_list_path = self.root/'val.txt'
        self.scenes = [self.root/folder[:-1] for folder in open(scene_list_path)]
        #self.imgs, self.depth = crawl_folders(self.scenes)
        self.transform = transform
        self.full_seq = full_seq
        self.crawl_folders(sequence_length)

    def crawl_folders(self, sequence_length):
        sequence_set = []
        # st()
        for scene in self.scenes:
            intrinsics = np.genfromtxt(scene/'cam.txt').astype(np.float32).reshape((3, 3))
            imgs = sorted(scene.files('*.jpg'))
            #depth_imgs = sorted(scene.files('*.npy'))
            if len(imgs) < sequence_length:
                continue
            for i in range(len(imgs)-sequence_length):
                sample = {'intrinsics': intrinsics, 'ref_imgs': [], 'tgt_depth':[]}
                for j in range(sequence_length):
                    sample['ref_imgs'].append(imgs[i+j])
                    if self.full_seq:
                        sample['tgt_depth'].append(sample['ref_imgs'][-1][:-4]+'.npy')
                #sample['tgt_depth'] = depth_imgs[i+sequence_length-1]
                if not self.full_seq:
                    sample['tgt_depth'] = sample['ref_imgs'][-1][:-4]+'.npy'
                sequence_set.append(sample)
        self.samples = sequence_set

    def __getitem__(self, index):
        sample = self.samples[index]
        ref_imgs = [load_as_float(ref_img) for ref_img in sample['ref_imgs']]
        if self.full_seq:
            depth_gt = [np.load(tgt_depth).astype(np.float32) for tgt_depth in sample['tgt_depth']]
        else:
            depth_gt = np.load(sample['tgt_depth']).astype(np.float32)
        if self.transform is not None:
            ref_imgs, intrinsics = self.transform(ref_imgs, np.copy(sample['intrinsics']))
        return ref_imgs, depth_gt, intrinsics

    def __len__(self):
        return len(self.samples)
